@extends('layout/main')

@section('title', "Form tambah Mahasiswa")

@section('container')
    <div class="container">
    <div class="row">
    <div class="col-8">
        <h1 class="mt-2">Form tambah Mahasiswa</h1>

        <form method="POST" action="/students">
        @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" placeholder="Masukan nama" value="{{old('nama')}}">
                @error('nama')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="nrp">NRP</label>
                <input type="text" class="form-control @error('nrp') is-invalid @enderror" name="nrp" id="nrp" placeholder="Masukan nrp" value="{{old('nrp')}}">
                @error('nrp')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="Masukan email" value="{{old('email')}}">
            </div>
            
            <div class="form-group">
                <label for="jurusan">Jurusan</label>
                <input type="text" class="form-control" name="jurusan" id="jurusan" placeholder="Masukan jurusan" value="{{old('jurusan')}}">
            </div>

            <button type="submit" class="btn btn-primary">Tambah Data</button>
</form>


    </div>
    </div>
    </div>
@endsection 

  